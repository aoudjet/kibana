#!/bin/sh
set -eu
echo Checking kibana setup...
mapmax=`cat /proc/sys/vm/max_map_count`
filemax=`cat /proc/sys/fs/file-max`
filedescriptors=`ulimit -n`
echo "fs.file_max: $filemax"
echo "vm.max_map_count: $mapmax"
echo "ulimit.file_descriptors: $filedescriptors"
fds=`ulimit -n`
if [ "$fds" -lt "64000" ] ; then
  echo " try \`ulimit -n 64000\`"
else
  echo "you have more than 64k allowed file descriptors. awesome"
fi
if [ "$1" = 'kibana' ]; then
  echo -e '\nStarting kibana...'
fi
exec "$@"
