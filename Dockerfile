FROM docker.elastic.co/kibana/kibana-oss:6.2.2
COPY kibana.yml /opt/kibana/config/kibana.yml
#RUN bin/kibana-plugin install https://github.com/prelert/kibana-swimlane-vis/releases/download/v5.5.1/prelert_swimlane_vis-5.5.1.zip
RUN bin/kibana-plugin install https://search.maven.org/remotecontent?filepath=com/floragunn/search-guard-kibana-plugin/6.2.2-10/search-guard-kibana-plugin-6.2.2-10.zip
#RUN bin/kibana-plugin remove x-pack 
USER root
RUN chmod 777 -R /usr/share/kibana